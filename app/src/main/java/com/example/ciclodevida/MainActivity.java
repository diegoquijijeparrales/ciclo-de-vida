package com.example.ciclodevida;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e("Ciclo de vida", "onCreate");
        MostrarMensaje("onCreate");
    }
    private void MostrarMensaje (String mensaje){
        /// toast mensajes de los metodos
        Toast.makeText(
                this, mensaje, Toast.LENGTH_LONG)
                .show();
    }

    //// metodos para mostrar

    @Override
    protected void onStart(){
        super.onStart();
        Log.e( "Ciclo de vida", "onStart");
        MostrarMensaje("onStart");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.e( "Ciclo de vida", "onResume");
        MostrarMensaje("onResume");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.e( "Ciclo de vida", "onPause");
        MostrarMensaje("onPause");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.e( "Ciclo de vida", "onStop");
        MostrarMensaje("onStop");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.e( "Ciclo de vida", "onDestroy");
        MostrarMensaje("onDestroy");
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Log.e( "Ciclo de vida", "onRestart");
        MostrarMensaje("onRestart");
    }
}